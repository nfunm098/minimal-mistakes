---
title:  "浅析微信界面设计"
modified: 2018-07-08 T16:0:49-04:00
categories: 
  - 界面设计
tags:
  - 笔记
classes: wide
header:
  overlay_image: /images/grape.jpeg
  caption: " [**Unsplash**](https://unsplash.com)"
  cta_label: "了解更多"
  cta_url: "https://unsplash.com"
sidebar:
  nav: "docs"
---

{% include base_path %}

{% include toc title="目录" %}



## 界面设计的第三篇笔记


![界面设计.jpg](https://upload-images.jianshu.io/upload_images/9472238-524f4b86f405600a.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
###  微信界面设计
微信很好用，其简洁且美观的界面设计功不可没。其中WeUI刮起互联网上的一种界面设计中的“暴力美学”之风。下面我们一起去看看。

![界面设计.png](https://upload-images.jianshu.io/upload_images/9472238-33352916fc65c546.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 导航栏设计

![微信界面设计.png](https://upload-images.jianshu.io/upload_images/9472238-86e0c686e5918d7c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 界面加载设计

![微信界面设计.png](https://upload-images.jianshu.io/upload_images/9472238-b9dc1b76d8ccab9c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 选项卡，面包屑导航的设计

### 心得：
看了上面几张图，根据所学习的界面设计知识，总结出了微信界面设计的如下优势。
- 整体风格一致，比如导航栏都在每一页界面的相同位置，这就培养了用户的惯性操作。
- 色彩上的突出对比，采用了冷暖对比，让用户在第一时间关注到重要信息。
- 图形图像的的适当应用，比如微信的图标设计。
- 个性化定制服务，又不同风格和可以修改聊天界面图片等。
-  界面设计总体上高端大气上档次，比如信息界面两端留白，界面分栏简洁，导航清晰分明，字体设置响应式。

总的来说，界面设计并不是很零散的分开的，更多的是一个涉及方方面面知识的一个系统。它充分运用我们生活中的各种知识，如色彩学，心理学等，给我们的用户最佳的体验，从而实现最终的目的。


