---
title:  "界面设计之交互"
modified: 2018-06-26 T16:03:49-04:00
categories: 
  - 界面设计
tags:
  - 笔记
classes: wide
header:
  overlay_image: /images/leaves.jpeg
  caption: " [**Unsplash**](https://unsplash.com)"
  cta_label: "了解更多"
  cta_url: "https://unsplash.com"
sidebar:
  nav: "docs"
---

{% include base_path %}

{% include toc title="目录" %}


## 界面设计的第二篇笔记

### 第一节 信息设计
![信息设计.jpg](https://upload-images.jianshu.io/upload_images/9472238-6069519cf4bf2467.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

一个网页由完整的页头页脚、导航栏、联系方式、LOGO等。用户也习惯于这样构成的界面，
然而在网页设计期间可能会有意无意地创建很多模块，版面或者元素，而它们的功能可能有些是重叠的。
这种网页其实是失败的，不符合我们信息设计的标准。

### 第二节 交互
UI就是User Interface(用户界面)，UI设计是指对软件的人机交互、操作逻辑、界面美观的整体设计。
一般我们所说的UI设计多指UI视觉设计，主要负责APP、Web、H5等页面的色彩、布局、icon、字体方面的设计工作。——来自百度百科
- 清晰
通过使用文字、流程图、图标等元素，避免用户对界面的模糊认识。
清晰的界面不需要使用手册，也能确保用户在使用过程中减少犯错。
- 简洁
为了让界面清晰，可能会加上浮动框说明或标签提示，导致界面臃肿，因此要让界面清晰的同时考虑界面简洁。
- 熟悉
用户在第一次使用界面时，仍然能发现某些元素是他们所熟知的。在设计中，使用一些现实生活中公认的意象能够更好地帮助用户理解。
例如现在比较流行的抽屉式导航借鉴的就是现实中的抽屉，将最主要的信息显示在界面上，
而将非核心的信息隐藏，需要的时候往右一滑，打开抽屉。

![抽屉设计.jpg](https://upload-images.jianshu.io/upload_images/9472238-7d878057ad0cf541.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

#### 二八法则：20%的网站功能占用了用户80%的时间
因此我们需要明确是为什么用户提供服务和什么是产品的重要功能，尽可能去简单方便的提供给我们的用户，达到好的交互效果。
#### 总的来说，好的界面设计，一方面需要固守用户原本对网页的认知，另一方面也要创新一些新的设计去达到更便捷的效果。做网页设计，始终要以用户为优先原则，让用户愿意再次光临我们的网站。

