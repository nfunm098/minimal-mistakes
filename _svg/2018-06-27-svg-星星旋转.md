---
title:  "SVG—星星动画"
modified: 2018-07-08T16:03:49-04:00
categories: 
  - 网页设计
tags:
  - svg
classes: wide
header:
  overlay_image: /images/stamen.jpeg
  caption: " [**Unsplash**](https://unsplash.com)"
  cta_label: "了解更多"
  cta_url: "https://unsplash.com"
sidebar:
  nav: "docs"
---

{% include base_path %}

{% include toc title="Getting Started" %} 

## 星星旋转围绕无形的圆

<svg width="800" height="800">
    <path d="M100 100, A120 120, -45 0 1, 300 300 A120 120, -45 0 1, 100 100"
          stroke="#dr4321" stroke-width="3"
          fill="none"/>
    <polygon points="-12 -69,-58 85,64 -14,-81 -14,41 85" style="fill: #df5214;" >
        <animateMotion
                dur="6s"
                repeatCount="indefinite"
                path="M100 100, A120 120, -45 0 1, 300 300 A120 120, -45 0 1, 100 100"
                rotate="auto"
        />
    </polygon>
</svg>

### 心得
星星是采用了rotate动画，其实是围着圆在转。rotate=“auto”，效果一般。

