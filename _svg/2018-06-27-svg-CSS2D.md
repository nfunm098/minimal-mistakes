---
title:  "CSS的2D变形"
modified: 2018-07-12T16:08:49-04:00
categories: 
  - 网页设计
tags:
  - svg
classes: wide
header:
  overlay_image: /images/flowers.jpeg
  caption: " [**Unsplash**](https://unsplash.com)"
  cta_label: "了解更多"
  cta_url: "https://unsplash.com"
sidebar:
  nav: "docs"
---

{% include base_path %}

{% include toc title="Getting Started" %}


<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="-200 -200 800 800">
     <rect x="50" y ="50" width="50" height="50" fill="red">
          <animateTransform attributeName="transform"
               attributeType="XML"
               type="rotate"
               from="0 75 75"
               to="360 75 75"
               dur="2"
               repeatCount="indefinite"/>
     </rect>
     <rect x="50" y ="150" width="50" height="50" fill="green">
          <animateTransform attributeName="transform"
               attributeType="XML"
               type="scale"
               from="1"
               to="2"
               dur="2"
               fill="freeze"/>
     </rect>     
     <rect x="50" y ="250" width="50" height="50" fill="blue">
          <animateTransform attributeName="transform"
               attributeType="XML"
               type="translate"
               to="250 0"
               dur="2"
               fill="freeze"/>
     </rect>
     <rect x="50" y ="150" width="50" height="50" fill="yellow">
          <animateTransform attributeName="transform"
               attributeType="XML"
               type="rotate"
               from="0 75 125"
               to="360 75 125"
               dur="2"
               repeatCount="indefinite" additive="sum"/>
          <animateTransform attributeName="transform"
               attributeType="XML"
               type="scale"
               from="1"
               to="2"
               dur="2"
               fill="freeze" additive="sum"/>
     </rect>
     <rect x="150" y="150" width="100" height="100" fill="purple"
           <animateTransform attributeName="transform"
                attributeType="XML"
                type="skew"
                to="150 0"
                dur="2"
                fill="freeze"/>
     </rect>
</svg>

- scale:用来缩放元素
- translate:在屏幕上移动元素(上下左右）
- rotate:按照一定角度旋转元素（单位为度）  
- skew: 沿X轴和Y轴进行斜切
- matrix:允许你以像素精度来控制变形效果
